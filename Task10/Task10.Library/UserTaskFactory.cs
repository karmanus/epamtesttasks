﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Task10.Library
{
    public class UserTaskFactory
    {
        static Random rnd = new Random();
        public  Task CreateTask(CancellationToken token)
        {
            return Task.Run(() => WorkSimulate(token), token);
        }

        private void WorkSimulate(CancellationToken token)
        {
            var interations = rnd.Next(5, 30);
            for (int i = 0; i < interations; i++)
            {
                if (token.IsCancellationRequested)
                {
                    Console.WriteLine("Canceled");
                    return;
                }
                Console.WriteLine(i);
                Thread.Sleep(rnd.Next(200, 300));
            }
            Console.WriteLine("Completed");
        }
    }
}
