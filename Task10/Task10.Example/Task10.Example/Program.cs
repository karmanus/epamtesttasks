﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Task10.Library;

namespace Task10.Example
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            await Example1();
            await Example2();
            await Example3();
            Thread.Sleep(500);
        }

        public async static Task Example1()
        {
            UserTaskFactory factory = new UserTaskFactory();
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource(2000);
            var task = factory.CreateTask(cancellationTokenSource.Token);
            await task;
        }

        public async static Task Example2()
        {
            UserTaskFactory factory = new UserTaskFactory();
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            List<Task> tasks = new List<Task>();
            for (int i = 0; i < 10; i++)
            {
                tasks.Add(factory.CreateTask(cancellationTokenSource.Token));
            }
            await Task.WhenAll(tasks);
        }

        public async static Task Example3()
        {
            UserTaskFactory factory = new UserTaskFactory();
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            List<Task> tasks = new List<Task>();
            for (int i = 0; i < 10; i++)
            {
                tasks.Add(factory.CreateTask(cancellationTokenSource.Token));
            }
            await Task.WhenAny(tasks);
            cancellationTokenSource.Cancel();
        }
    }
}
