﻿using System.Collections.Generic;
using System.Linq;

namespace Task5.Library
{
    public class DfsSearch
    {
        private Node _tree;
        private Dictionary<int, Node> _nodeList;

        public DfsSearch(Node tree)
        {
            _tree = tree;
            _nodeList = new Dictionary<int, Node>();
            var count = 1;
            WalkOnTree(tree, ref count);
        }

        public Node this[int index]
        {
            get
            {
                _nodeList.TryGetValue(index, out var val);
                return val;
            }
        }

        public Node this[string val]
        {
            get => _nodeList.Single(x => x.Value.Val == val).Value;
        }

        private void WalkOnTree(Node node, ref int count)
        {
            if (node == null)
            {
                return;
            }

            _nodeList.Add(count, node);
            count++;

            WalkOnTree(node.Left, ref count);
            WalkOnTree(node.Right, ref count);
        }
    }
}
