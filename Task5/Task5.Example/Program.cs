﻿using System;
using Task5.Library;

namespace Task5.Example
{
    class Program
    {
        static void Main(string[] args)
        {
            var tree = new Node(new Node(new Node(new Node(new Node(null, null, "5"), new Node(null, null, "6"), "4"), new Node(new Node(null, null, "8"),
                new Node(null, null, "9"), "7"), "3"), new Node(new Node(new Node(null, null, "12"), new Node(null, null, "13"), "11"), new Node(new Node(null, null, "15"), new Node(null, null, "16"),
                "14"), "10"), "2"), new Node(new Node(new Node(null, null, "19"), new Node(null, null, "20"), "18"), new Node(null, null, "21"), "17"), "1");

            var dfsSearch = new DfsSearch(tree);
            Console.WriteLine(dfsSearch[22]?.Val);
            Console.WriteLine(dfsSearch["4"].Val);
            Console.WriteLine(dfsSearch["4"].Val);
            Console.ReadKey();
        }
    }
}
