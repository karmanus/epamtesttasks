Create a console calculator:

Application Spec:

Input:
	param1: -10000.00000 <= x <= 10000.00000
	param2: -10000.00000 <= y <= 10000.00000
	param3: Operation ∈ {'M', 'D', 'A', 'S'}
	'A' - addition
	'D' - division
	'M' - multiplication
	'S' - subtraction

Output:
	Result of the operation.
	Accuracy is 5 digits after dot.
	If the input params don't fit within specifed range or params count is wrong - 'error'.

Samples:
	ConsoleCalculator.exe 100 20 M
	2000
	ConsoleCalculator.exe -100.00 -2 D
	50
	ConsoleCalculator.exe -423.773 -2000.12342 S
	1576.35042
	ConsoleCalculator.exe -10000.00001 S
	error