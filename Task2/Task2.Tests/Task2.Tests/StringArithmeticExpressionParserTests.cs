﻿using Task2.Library;
using Xunit;

namespace Task2.Tests
{
    public class StringArithmeticExpressionParserTests
    {
        [Fact]
        public void Parse_MultiplyAritmeticExpreession_Correct()
        {
            StringArithmeticExpressionParser parser = new StringArithmeticExpressionParser();

            var result = parser.Parse("5 4 M");

            Assert.Equal(5, result.LeftParameter);
            Assert.Equal(4, result.RightParameter);
            Assert.Equal(Operation.M, result.Operation);
        }

        [Fact]
        public void Parse_IncorrectThirdParrammeter_ThrowsIncorrectParametersExeption()
        {
            StringArithmeticExpressionParser parser = new StringArithmeticExpressionParser();

            //Acceptable values for the third parameter {M,D,S,A}
            Assert.Throws<IncorrectParametersOrTheirNumber>(() => parser.Parse("5 3 H"));
        }

        [Fact]
        public void Parse_FirstNumberOutRange_ThrowsOutRangeExeption()
        {
            StringArithmeticExpressionParser parser = new StringArithmeticExpressionParser();

            //The first and second parameters must be greater than -10000 and less than 10000
            Assert.Throws<InvalidParametrsRange>(() => parser.Parse("12200 3 A"));
        }

        [Fact]
        public void Pasre_SecondParameterMorethenFiveCharactersAfterDot_ThrowsIncorrectParametersOrTheirNumberException()
        {
            StringArithmeticExpressionParser parser = new StringArithmeticExpressionParser();

            //The first and second parameters must have less than 5 characters after dot
            Assert.Throws<IncorrectParametersOrTheirNumber>(() => parser.Parse("5.0000 5.322222 A"));
        }
    }
}
