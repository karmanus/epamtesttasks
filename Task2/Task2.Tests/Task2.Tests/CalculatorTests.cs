using Task2.Library;
using Xunit;

namespace Task2.Tests
{
    public class CalculatorTests
    {
        [Fact]
        public void Calculate_MultiplyAritmeticExpreession_Correct()
        {
            Calculator calculator = new Calculator();

            var result = calculator.Calculate(new ArithmeticOperation(5, 4, Operation.M));

            Assert.Equal(20, result);
        }

        [Fact]
        public void Calculate_ZeroDivisor_ThrowsDivisionException()
        {
            Calculator calculator = new Calculator();

            Assert.Throws<DivideByZero>(() => calculator.Calculate(new ArithmeticOperation(5, 0, Operation.D)));
        }

        [Fact]
        public void Caculate_MultiplyAritmeticExpreession_Correct()
        {
            Calculator calculator = new Calculator();

            var result = calculator.Calculate(new ArithmeticOperation(564, 745, Operation.S));

            Assert.Equal(-181, result);
        }
    }
}
