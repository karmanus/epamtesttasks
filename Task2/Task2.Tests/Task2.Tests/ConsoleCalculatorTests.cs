﻿using Moq;
using Task2.Library;
using Task2.Library.Interfaces;
using Xunit;

namespace Task2.Tests
{
    public class ConsoleCalculatorTests
    {
        [Fact]
        public void Calculate_CorrectAdditionArithmeticOperation_Correct()
        {
            ConsoleCalculator calculator = new ConsoleCalculator();
            Mock<IConsole> mock = new Mock<IConsole>();
            mock.Setup(m => m.Readline()).Returns("5 7 A");
            Assert.Equal(12, calculator.Calculate(mock.Object, new Calculator(), new StringArithmeticExpressionParser()));
        }
    }
}
