﻿namespace Task2.Library.Interfaces
{
    public interface ICalculator
    {
        float Calculate(ArithmeticOperation expression);
    }
}
