﻿namespace Task2.Library.Interfaces
{
    public interface IConsoleCalculator
    {
        float Calculate(IConsole console, ICalculator calculato, IArithmeticExpressionParser parser);
    }
}
