﻿namespace Task2.Library.Interfaces
{
    public interface IArithmeticExpressionParser
    {
        ArithmeticOperation Parse(string str);
    }
}
