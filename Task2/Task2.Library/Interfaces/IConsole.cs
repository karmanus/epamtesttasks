﻿namespace Task2.Library.Interfaces
{
    public interface IConsole
    {
        string Readline();
    }
}
