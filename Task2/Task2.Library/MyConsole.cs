﻿using System;
using Task2.Library.Interfaces;

namespace Task2.Library
{
    public class MyConsole : IConsole
    {
        public string Readline()
        {
            return Console.ReadLine();
        }
    }
}
