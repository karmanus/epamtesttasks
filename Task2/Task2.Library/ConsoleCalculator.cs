﻿using Task2.Library.Interfaces;

namespace Task2.Library
{
    public class ConsoleCalculator : IConsoleCalculator
    {

        public float Calculate(IConsole console, ICalculator calculator, IArithmeticExpressionParser parser)
        {
            return calculator.Calculate(parser.Parse(console.Readline()));
        }
    }
}
