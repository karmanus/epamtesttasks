﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Task2.Library.Interfaces;

namespace Task2.Library
{
    public class StringArithmeticExpressionParser : IArithmeticExpressionParser
    {
        private bool StringArithmeticExpressionValidation(string str)
        {
            string mask = @"^\d+(\.\d{0,5})?\s\d+(\.\d{0,5})?\s[ADMS]$";
            return Regex.IsMatch(str, mask);
        }

        private string[] ExtractParams(string str)
        {
            return str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public ArithmeticOperation Parse(string str)
        {
            if (!StringArithmeticExpressionValidation(str))
            {
                throw new IncorrectParametersOrTheirNumber();
            }
            else
            {
                var param = ExtractParams(str);
                float firstParam = float.Parse(param[0], CultureInfo.InvariantCulture);
                float secondParam = float.Parse(param[1], CultureInfo.InvariantCulture);

                if (!(-10000.00000 <= firstParam && firstParam <= 10000.00000 && -10000.00000 <= secondParam && secondParam <= 10000.00000))
                {
                    throw new InvalidParametrsRange();
                }
                else
                {
                    Operation operation;
                    Enum.TryParse<Operation>(param[2], out operation);
                    return new ArithmeticOperation(firstParam, secondParam, operation);
                }
            }
        }
    }
}
