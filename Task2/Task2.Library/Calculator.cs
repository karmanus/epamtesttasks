﻿using System;
using Task2.Library.Interfaces;

namespace Task2.Library
{
    public class Calculator : ICalculator
    {
        public float Calculate(ArithmeticOperation expression)
        {
            switch (expression.Operation)
            {
                case Operation.А: return expression.LeftParameter + expression.RightParameter;
                case Operation.D:
                    if (expression.RightParameter == 0)
                    {
                        throw new DivideByZero();
                    }
                    return expression.LeftParameter / expression.RightParameter;
                case Operation.M: return expression.LeftParameter * expression.RightParameter;
                case Operation.S: return expression.LeftParameter - expression.RightParameter;
            }
            throw new Exception("Invalid operation");
        }
    }
}
