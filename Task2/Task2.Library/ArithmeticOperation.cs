﻿namespace Task2.Library
{
    public class ArithmeticOperation
    {
        public float LeftParameter { get; }
        public float RightParameter { get; }
        public Operation Operation { get; }

        public ArithmeticOperation(float leftParameter, float rightParameter, Operation operation)
        {
            LeftParameter = leftParameter;
            RightParameter = rightParameter;
            Operation = operation;
        }
    }
}
