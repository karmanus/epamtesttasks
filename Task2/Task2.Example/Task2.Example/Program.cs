﻿using System;
using Task2.Library;
using Task2.Library.Interfaces;

namespace Task2.Example
{
    class Program
    {
        static void Main(string[] args)
        {
            IConsoleCalculator consoleCalculator = new ConsoleCalculator();

            try
            {
                Console.WriteLine(consoleCalculator.Calculate(new MyConsole(), new Calculator(), new StringArithmeticExpressionParser()));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
