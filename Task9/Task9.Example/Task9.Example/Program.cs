﻿using System;
using Task9.Library.Interfaces;
using Task9.Library;

namespace Task9.Example
{
    class Program
    {
        static void Main(string[] args)
        {
            IStringParser stringParser = new StringParser();
            stringParser.OnStringContainsBLetter += Sp_OnStringContainsBLetter;
            stringParser.OnStringContainsZLetter += Sp_OnStringContainsZLetter;
            stringParser.Parse("vb|bBZZzz23z  bb2");
            Console.ReadKey();
        }

        private static void Sp_OnStringContainsZLetter(object sender, StringEventArguments e)
        {
            Console.WriteLine(sender.ToString() + ": " + e.Letter + " " + e.Count + " time");
        }

        private static void Sp_OnStringContainsBLetter(object sender, StringEventArguments e)
        {
            Console.WriteLine(sender.ToString() + ": " + e.Letter + " " + e.Count + " time");
        }
    }
}
