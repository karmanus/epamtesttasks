﻿using static Task9.Library.StringDelegate;

namespace Task9.Library.Interfaces
{
    public interface IStringParser
    {
        event StringEventHandler OnStringContainsBLetter;
        event StringEventHandler OnStringContainsZLetter;

        void Parse(string str);
    }
}
