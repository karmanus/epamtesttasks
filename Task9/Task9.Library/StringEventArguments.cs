﻿namespace Task9.Library
{
    public class StringEventArguments
    {
        public char Letter { get; set; }
        public int Count { get; set; }
    }
}
