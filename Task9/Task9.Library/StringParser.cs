﻿using Task9.Library.Interfaces;
using static Task9.Library.StringDelegate;

namespace Task9.Library
{
    public class StringParser : IStringParser
    {
        public event StringEventHandler OnStringContainsBLetter;
        public event StringEventHandler OnStringContainsZLetter;

        public void Parse(string str)
        {
            int bCount = 0;
            int zCount = 0;
            foreach (var letter in str)
            {
                if ((letter == 'b' || letter == 'B') && OnStringContainsBLetter != null)
                {
                    bCount += 1;
                    OnStringContainsBLetter(this, new StringEventArguments() { Letter = letter, Count = bCount });
                }
                else
                if ((letter == 'z' || letter == 'Z') && OnStringContainsBLetter != null)
                {
                    zCount += 1;
                    OnStringContainsZLetter(this, new StringEventArguments() { Letter = letter, Count = zCount });
                }
            }
        }
    }
}
