﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AsyncIO
{
    public static class Tasks
    {
        public static IEnumerable<string> GetUrlContent(this IEnumerable<Uri> uris)
        {
            List<string> resultList = new List<string>();
            WebClient wbClient = new WebClient();
            foreach (var uri in uris)
            {
                resultList.Add(wbClient.DownloadString(uri));
            }
            return resultList;
        }

        /// <summary>
        /// Returns the content of required uris.
        /// Method has to use the asynchronous way and can be used to compare the performace of sync \ async approaches. 
        /// 
        /// maxConcurrentStreams parameter should control the maximum of concurrent streams that are running at the same time (throttling). 
        /// </summary>
        /// <param name="uris">Sequence of required uri</param>
        /// <param name="maxConcurrentStreams">Max count of concurrent request streams</param>
        /// <returns>The sequence of downloaded url content</returns>

        public static IEnumerable<string> GetUrlContentAsync(this IEnumerable<Uri> uris, int maxConcurrentStreams)
        {
            return GetUrlContentAsyncHelper(uris, maxConcurrentStreams).Result;
        }

        public static async Task<List<string>> GetUrlContentAsyncHelper(IEnumerable<Uri> uris, int maxConcurrentStreams)
        {
            List<string> concurentTasks = new List<string>();

            List<Task<string>> tasks = new List<Task<string>>(maxConcurrentStreams);
            List<Uri> uriList = uris.ToList();
            if (uriList == null || uriList.Count() < 1)
            {
                return new List<string>() { string.Empty };
            }
            if (uriList.Count < maxConcurrentStreams)
            {
                uriList.Take(uriList.Count).ToList().ForEach(uri => tasks.Add(CreateUriContentTask(uri)));
            }
            else
            {
                uriList.Take(maxConcurrentStreams).ToList().ForEach(uri => tasks.Add(CreateUriContentTask(uri)));
            }

            uriList.RemoveRange(0, maxConcurrentStreams - 1);

            do
            {
                var overTask = await Task.WhenAny(tasks);
                concurentTasks.Add(await overTask);
                tasks.Remove(overTask);
                if (uriList.Count() > 0)
                {
                    tasks.Add(CreateUriContentTask(uriList.First()));
                    uriList.RemoveAt(0);
                }
            } while (tasks.Count() > 0);

            return concurentTasks;
        }

        public static Task<string> CreateUriContentTask(Uri uri)
        {
            return new WebClient().DownloadStringTaskAsync(uri);
        }
    }
}
