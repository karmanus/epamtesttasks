﻿using System;
using Task3.Library;

namespace Task3.Example
{
    class Program
    {
        static void Main(string[] args)
        {
            var m1 = new Money(100, Currencies.USD);
            var m2 = new Money(200, Currencies.EUR);

            var m3 = m1 + m2; // 350 USD
            var m4 = m2 + m1; // 280 EUR

            var m5 = new Money(80, Currencies.EUR);
            var m6 = new Money(100, Currencies.BYN);

            Console.WriteLine("m3: " + m3.Amount);
            Console.WriteLine("m4: " + m4.Amount);

            Console.WriteLine(m5 == m1);
            Console.WriteLine(m5 == m2);

            Console.WriteLine((string)m1);

            int cash = m6;

            Console.WriteLine(cash);

            Console.ReadKey();
        }
    }
}
