﻿using System;

namespace Task3.Library
{
    public class Money
    {
        public Currencies Currency { get; }
        public double Amount { get; }
        public double Ratio { get; }

        public Money(double amount, Currencies currency)
        {
            Currency = currency;
            Amount = amount;
            switch (currency)
            {
                case Currencies.USD:
                    Ratio = 1;
                    break;

                case Currencies.EUR:
                    Ratio = 0.8;
                    break;

                case Currencies.BYN:
                    Ratio = 2;
                    break;
            }
        }

        public static Money operator ++(Money money)
        {
            switch (money.Currency)
            {
                case Currencies.USD:
                    return new Money(money.Amount * 1.1, money.Currency);

                case Currencies.EUR:
                    return new Money(money.Amount * 1.2, money.Currency);
                case Currencies.BYN:
                    return new Money(money.Amount * 1.3, money.Currency);
            }
            return money;
        }

        public static double CastingСurrency(Money money1, Money money2)
        {
            return money1.Ratio / money2.Ratio * money2.Amount;
        }

        public static Money operator +(Money money1, Money money2)
        {
            double сastingСurrency = CastingСurrency(money1, money2);
            return new Money(money1.Amount + сastingСurrency, money1.Currency);
        }

        public static Money operator -(Money money1, Money money2)
        {
            double сastingСurrency = CastingСurrency(money1, money2);
            return new Money(money1.Amount - сastingСurrency, money1.Currency);
        }
        public static bool operator ==(Money money1, Money money2)
        {
            double сastingСurrency = CastingСurrency(money1, money2);
            if (money1.Amount == сastingСurrency)
            {
                return true;
            }
            return false;
        }

        public static bool operator !=(Money money1, Money money2)
        {
            double сastingСurrency = CastingСurrency(money1, money2);
            if (money1.Amount == сastingСurrency)
            {
                return false;
            }
            return true;
        }

        public static explicit operator string(Money money)
        {
            var amount = String.Format("{0:0.00000}", money.Amount);
            var currency = money.Currency.ToString();
            string str = $"{amount}_{currency}";
            return str;
        }

        public static implicit operator int(Money money)
        {
            int сastingСurrency = (int)((2 / money.Ratio) * money.Amount);
            return сastingСurrency;
        }
    }
}
