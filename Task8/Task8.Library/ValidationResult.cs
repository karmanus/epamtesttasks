﻿using System.Collections.Generic;

namespace Task8.Library
{
    public struct ValidationResult<T>
    {
        public T entity { get; set; }
        public bool IsValid { get; set; }
        public IEnumerable<string> ErrorMessages { get; set; }

        public ValidationResult(T entity, bool isValid, IEnumerable<string> errorMessages)
        {
            this.entity = entity;
            IsValid = isValid;
            ErrorMessages = errorMessages;
        }
    }
}
