﻿namespace Task8.Library
{
    public class Triangle
    {
        public string Name { get; }

        public double Area { get; }

        public Triangle(string name, double area)
        {
            Name = name;
            Area = area;
        }
    }
}
