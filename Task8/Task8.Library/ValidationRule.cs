﻿using System;

namespace Task8.Library
{
    public class ValidationRule<T>
    {
        public Predicate<T> IsValid { get; set; }
        public Func<T, string> ErrorMessage { get; set; }
    }
}
