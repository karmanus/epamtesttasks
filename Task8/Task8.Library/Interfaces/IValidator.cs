﻿namespace Task8.Library.Interfaces
{
    public interface IValidator<T>
    {
        ValidationResult<T> Validate(T entity);
    }
}

