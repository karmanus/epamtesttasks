﻿using System;

namespace Task8.Library.Interfaces
{
    public interface IValidatorBuilder<T>
    {
        IValidatorBuilder<T> AddRule(Predicate<T> IsValid, Func<T, string> errorMesage);
        IValidator<T> Build();
    }
}
