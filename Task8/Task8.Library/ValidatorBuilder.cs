﻿using System;
using System.Collections.Generic;
using Task8.Library.Interfaces;

namespace Task8.Library
{
    public class ValidatorBuilder<T> : IValidatorBuilder<T>
    {
        private List<ValidationRule<T>> _rules;

        public ValidatorBuilder()
        {
            _rules = new List<ValidationRule<T>>();
        }

        public IValidatorBuilder<T> AddRule(Predicate<T> isValid, Func<T, string> errorMesage)
        {
            _rules.Add(new ValidationRule<T> { IsValid = isValid, ErrorMessage = errorMesage });
            return this;
        }

        public IValidator<T> Build()
        {
            return new Validator<T>(_rules);
        }
    }
}
