﻿using System.Collections.Generic;
using Task8.Library.Interfaces;

namespace Task8.Library
{
    public class Validator<T> : IValidator<T>
    {
        private List<ValidationRule<T>> _rules;
        public Validator(List<ValidationRule<T>> rules)
        {
            _rules = rules;
        }
        public ValidationResult<T> Validate(T entity)
        {
            bool isFullValidation = true;
            List<string> errors = new List<string>();

            foreach (var rule in _rules)
            {
                var isValid = rule.IsValid(entity);
                if (!isValid)
                {
                    isFullValidation = false; ;
                    errors.Add(rule.ErrorMessage(entity));
                }
            }
            return new ValidationResult<T>(entity, isFullValidation, errors);
        }
    }
}
