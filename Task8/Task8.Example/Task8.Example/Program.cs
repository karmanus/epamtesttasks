﻿using System;
using Task8.Library;

namespace Task8.Example
{
    class Program
    {
        static void Main(string[] args)
        {
            var triangleValidator = new ValidatorBuilder<Triangle>()
       .AddRule(t => t.Name == "Triangle", t => $"Name {t.Name} is not correct")
       .AddRule(t => t.Area > 20, t => $"Area value {t.Area} is not correct. Should be more than 20")
       .Build();

            var validationResult = triangleValidator.Validate(new Triangle("Triangl", 19));
            if (!validationResult.IsValid)
            {
                Console.WriteLine(string.Join(Environment.NewLine, validationResult.ErrorMessages));
            }
        }
    }
}
