﻿namespace Task4.Library
{
    public class Rectangle : Shape
    {
        public double SideA { get; }
        public double SideB { get; }

        public Rectangle(double sideA, double sideB, string name) : base(name)
        {
            SideA = sideA;
            SideB = sideB;
        }
        public override double GetArea()
        {
            return SideA * SideB;
        }
    }
}
