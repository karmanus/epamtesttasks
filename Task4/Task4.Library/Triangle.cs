﻿namespace Task4.Library
{
    public class Triangle : Shape
    {
        public double Height { get; }
        public double BaseSide { get; }

        public Triangle(double height, double baseSide, string name) : base(name)
        {
            Height = height;
            BaseSide = baseSide;
        }

        public override double GetArea()
        {
            return Height * BaseSide / 2;
        }
    }
}
