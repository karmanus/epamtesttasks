﻿using System.Collections.Generic;
using System.Linq;

namespace Task4.Library
{
    public class ShapeAreaCalculator : IShapeAreaCalculator
    {
        public double Calculate(IEnumerable<Shape> shapes)
        {
            return shapes.Sum(shape => shape.GetArea());
        }
    }
}
