﻿using System;

namespace Task4.Library
{
    public class Circle : Shape
    {
        public double Radius { get; }
        public Circle(double radius, string name) : base(name)
        {
            Radius = radius;
        }

        public override double GetArea()
        {
            return Math.PI * Radius * Radius;
        }
    }
}
