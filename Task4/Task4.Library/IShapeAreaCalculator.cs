﻿using System.Collections.Generic;

namespace Task4.Library
{
    public interface IShapeAreaCalculator
    {
        double Calculate(IEnumerable<Shape> shapes);
    }
}