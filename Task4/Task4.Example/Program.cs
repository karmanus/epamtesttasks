﻿using System;
using System.Collections.Generic;
using Task4.Library;

namespace Task4.Example
{
    class Program
    {
        static void Main(string[] args)
        {
            IShapeAreaCalculator areaCalculator = new ShapeAreaCalculator();

            List<Shape> shapes = new List<Shape>() {new Rectangle(5,10,"Прямоугольничек"),
                                                    new Circle(4.5, "Кружочек"),
                                                    new Triangle(3,5,"Треугольничек")
            };
            Console.WriteLine(areaCalculator.Calculate(shapes));
            Console.ReadKey();
        }
    }
}

