1.) Create 3 classes Triangle, Rectangle, Circle(use Shape class as a base class)
2.) Override GetArea method according to shape type.
3.) Implement IShapeAreaCalculator interface

----------------------------

public interface IShapeAreaCalculator
{
    double Calculate(IEnumerable<Shape> shapes); // returns a sum of all shapes areas
}

public abstract class Shape
{
    public string Name { get; }

    public Shape(string name) => Name = name;

    public abstract double GetArea();
}

Example:

{
	IShapeAreaCalculator areaCalculator = new ShapeAreaCalculator();

	var shapes = new List<Shape> 
	{
		new Triangle(a, h),
		new Circle(d),
		..........
		
	}

	areaCalculator.Calculate(shapes);
}



