﻿using System;
using Task7.Library;

namespace Task7.Example
{
    class Program
    {
        static void Main(string[] args)
        {
            Database data = new Database("data");
            User user = new User();
            ManagerUser manager = new ManagerUser();
            AdminUser admin = new AdminUser();
            user.UseDatabase(data);
            manager.UseDatabase(data);
            admin.UseDatabase(data);
            Console.WriteLine(data.Shema);
        }
    }
}
