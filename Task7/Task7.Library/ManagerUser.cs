﻿using Task7.Library.Interfaces;

namespace Task7.Library
{

    public class ManagerUser : IDbUser<IReadUpdateDatabase>
    {
        public void UseDatabase(IReadUpdateDatabase db)
        {
            db.ReadData();
            db.UpdateData(data => data += "M");
        }
    }
}
