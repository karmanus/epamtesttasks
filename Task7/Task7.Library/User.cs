﻿using Task7.Library.Interfaces;

namespace Task7.Library
{
    public class User : IDbUser<IReadDatabase>
    {
        public void UseDatabase(IReadDatabase db)
        {
            db.ReadData();
        }
    }
}
