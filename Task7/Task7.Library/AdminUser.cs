﻿using Task7.Library.Interfaces;

namespace Task7.Library
{
    public class AdminUser : IDbUser<IReadUpdateWriteDatabase>
    {
        public void UseDatabase(IReadUpdateWriteDatabase db)
        {
            db.ReadData();
            db.UpdateData(data => data += "a");
            db.WriteData("Admin was here");
        }
    }
}
