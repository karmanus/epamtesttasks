﻿using System;

namespace Task7.Library.Interfaces
{
    public interface IReadUpdateDatabase : IDatabase
    {
        string ReadData();
        void UpdateData(Action<string> update);
    }
}
