﻿namespace Task7.Library.Interfaces
{
    interface IDbUser<T> where T : IDatabase
    {
        void UseDatabase(T db);
    }
}
