﻿namespace Task7.Library.Interfaces
{
    public interface IReadDatabase : IDatabase
    {
        string ReadData();
    }
}
