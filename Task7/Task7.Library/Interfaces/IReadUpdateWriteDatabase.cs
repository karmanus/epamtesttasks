﻿using System;

namespace Task7.Library.Interfaces
{
    public interface IReadUpdateWriteDatabase : IDatabase
    {
        string ReadData();
        void WriteData(string data);
        void UpdateData(Action<string> update);
    }
}
