﻿using System;
using Task7.Library.Interfaces;

namespace Task7.Library
{
    public class Database : IReadDatabase, IReadUpdateDatabase, IReadUpdateWriteDatabase
    {
        public string Shema { get; private set; }

        public Database(string shema)
        {
            Shema = shema;
        }

        public string ReadData()
        {
            return Shema;
        }

        public void UpdateData(Action<string> update)
        {
            update(Shema);
        }

        public void WriteData(string data)
        {
            Shema = data;
        }
    }
}
