﻿using System;
using System.Threading;
using Task6.Library;

namespace Task6.Example
{
    class Program
    {
        static void Main(string[] args)
        {
            ICache<int, string> cache = new Cache<int, string>();

            cache.AddOrUpdate(1, "Value1", DateTime.Now.AddSeconds(5));
            cache.AddOrUpdate(2, "Value2", DateTime.Now.AddSeconds(5));
            cache.AddOrUpdate(3, "Value3", DateTime.Now.AddSeconds(5));
            cache.AddOrUpdate(4, "Value4", DateTime.Now.AddSeconds(5));
            cache.AddOrUpdate(1,"new value", DateTime.Now.AddSeconds(5));

            Console.WriteLine(cache.Get(1));

            Console.WriteLine(cache.Remove(5));
            Console.WriteLine(cache.Get(4));

            Thread.Sleep(5000);

            Console.WriteLine(cache.Get(4));
            Console.ReadKey();
        }
    }
}
