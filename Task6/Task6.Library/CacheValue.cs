﻿using System;

namespace Task6.Library
{
    public class CacheValue<T> where T : class
    {
        public DateTime ValideDataTime { get; set; }
        public T Value { get; set; }

        public CacheValue(T value, DateTime valideDataTime)
        {
            ValideDataTime = valideDataTime;
            Value = value;
        }
    }
}
