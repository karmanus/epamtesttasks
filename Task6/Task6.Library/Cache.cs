﻿using System;
using System.Collections.Generic;

namespace Task6.Library
{
    public class Cache<TKey, TValue> : ICache<TKey, TValue>
         where TKey : struct
         where TValue : class
    {
        private IDictionary<TKey, CacheValue<TValue>> _cacheList;

        public Cache()
        {
            _cacheList = new Dictionary<TKey, CacheValue<TValue>>();
        }

        public void AddOrUpdate(TKey key, TValue value, DateTime expiresOn)
        {
            if (!_cacheList.ContainsKey(key))
            {
                _cacheList.Add(key, new CacheValue<TValue>(value, expiresOn));
            }
            else
            {
                var pair = _cacheList[key];
                pair.Value = value;
                pair.ValideDataTime = expiresOn;
            }
        }

        public TValue Get(TKey key)
        {
            if (_cacheList.ContainsKey(key))
            {
                var pair = _cacheList[key];
                if (pair.ValideDataTime <= DateTime.Now)
                {
                    return null;
                }
                else
                {
                    return pair.Value;
                }
            }
            else
            {
                return null;
            }
        }

        public bool Remove(TKey key)
        {
            return _cacheList.Remove(key);
        }
    }
}
