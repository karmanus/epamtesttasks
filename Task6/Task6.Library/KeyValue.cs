﻿namespace Task6.Library
{
    public class KeyValue<TKey, TValue>
         where TKey : struct
         where TValue : class
    {
        public TKey Key { get; }
        public TValue Value { get; }

        public KeyValue(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }
    }
}
