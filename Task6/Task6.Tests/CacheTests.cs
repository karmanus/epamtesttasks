using System;
using System.Threading;
using Task6.Library;
using Xunit;

namespace Task6.Tests
{
    public class CacheTests
    {
        [Fact]
        public void AddOrUpdate_NewCacheValue_AddsValue()
        {
            Cache<int, string> cache = new Cache<int, string>();
            cache.AddOrUpdate(1, "2", DateTime.Now.AddMilliseconds(1));
        }

        [Fact]
        public void Get_ActualCacheValue_GetActualValue()
        {
            Cache<int, string> cache = new Cache<int, string>();
            cache.AddOrUpdate(1, "2", DateTime.Now.AddSeconds(20));
            var result = cache.Get(1);
            Assert.Equal("2", result);
        }

        [Fact]
        public void Get_KeyNonExistentCacheValue_ReturnsNull()
        {
            Cache<int, string> cache = new Cache<int, string>();
            var result = cache.Get(1);
            Assert.Null(result);
        }

        [Fact]
        public void AddOrUpdate_ExistingCacheValue_Updates()
        {
            Cache<int, string> cache = new Cache<int, string>();
            cache.AddOrUpdate(1, "2", DateTime.Now.AddSeconds(10));
            cache.AddOrUpdate(1, "3", DateTime.Now.AddSeconds(10));
            Assert.Equal("3", cache.Get(1));
        }

        [Fact]
        public void Get_IsntActualCacheValue_ReturnsNull()
        {
            Cache<int, string> cache = new Cache<int, string>();
            cache.AddOrUpdate(1, "2", DateTime.Now);
            Thread.Sleep(1);
            var result = cache.Get(1);
            Assert.Null(result);
        }

        [Fact]
        public void Remove_RemovingCacheValueKey_RemoveCacheValue()
        {
            Cache<int, string> cache = new Cache<int, string>();
            cache.AddOrUpdate(1, "2", DateTime.Now);
            var result = cache.Remove(1);
            Assert.True(result);
        }
    }
}
